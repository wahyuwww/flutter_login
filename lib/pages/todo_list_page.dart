// ignore_for_file: prefer_const_constructors, prefer_is_empty, use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:flutter/material.dart';

class TodoListPage extends StatefulWidget {
  @override
  _TodoListPageState createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  List<String> todoList = [];

  final TextEditingController todoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('To Do List Page'),
        actions: [
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/login');
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
        child: Column(
          children: [
            TextField(
              controller: todoController,
              decoration: InputDecoration(
                labelText: 'Add Todo',
                prefixIcon: Icon(Icons.add),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                todoController.text.length > 0 ? addTodo() : null;
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blue, // Background color
                onPrimary: Colors.white, // Text color
                elevation: 8, // Elevation (shadow)
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10), // Border radius
                ),
                padding: EdgeInsets.symmetric(
                    horizontal: 16, vertical: 15), // Padding
              ),
              child: Text(
                'Add Todo',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(height: 20),
            todoList.isEmpty
                ? _dataEmpty()
                : Expanded(
                    child: ListView.builder(
                      itemCount: todoList.length,
                      itemBuilder: (context, index) {
                        return Card(
                          elevation: 3.5,
                          margin:
                              EdgeInsets.symmetric(horizontal: 3, vertical: 10),
                          child: ListTile(
                            title: Text(todoList[index]),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                IconButton(
                                  icon: const Icon(Icons.edit),
                                  color: Colors.yellow,
                                  onPressed: () {
                                    // Tampilkan dialog untuk mengedit todo
                                    editTodo(index);
                                  },
                                ),
                                IconButton(
                                  icon: const Icon(Icons.delete),
                                  color: Colors.red,
                                  onPressed: () {
                                    // Hapus todo dari daftar
                                    deleteTodo(index);
                                  },
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Widget _dataEmpty() {
    return Center(
      child: Text(
        'No Todo',
        style: TextStyle(
          fontSize: 20,
          height: 15,
        ),
      ),
    );
  }

  void addTodo() {
    if (todoController.text.length > 0) {
      setState(() {
        todoList.add(todoController.text);
        todoController.clear();
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("No Text")),
      );
    }
  }

  void editTodo(int index) async {
    TextEditingController editingController =
        TextEditingController(text: todoList[index]);

    String? editedTodo;
    editedTodo = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Edit Todo'),
          content: TextField(
            controller: editingController,
            onChanged: (value) {
              editedTodo = value;
            },
            // decoration: InputDecoration(labelText: 'Edit Todo'),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            ElevatedButton(
              onPressed: () {
                editedTodo != null
                    ? editedTodo!.length > 0
                        ? Navigator.of(context).pop(editedTodo)
                        : ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("No Text")),
                          )
                    : Navigator.of(context).pop(editedTodo);
              },
              child: const Text('Save'),
            ),
          ],
        );
      },
    );

    if (editedTodo != null) {
      setState(() {
        todoList[index] = editedTodo!;
      });
    }
  }

  void deleteTodo(int index) {
    setState(() {
      todoList.removeAt(index);
    });
  }
}
